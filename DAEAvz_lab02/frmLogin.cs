﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAEAvz_lab02
{
    public partial class frmLogin : Form
    {
        //atributos
        private Dictionary<string, string> users;

        public frmLogin()
        {
            InitializeComponent();

            //crea usuarios de prueba para validar el login
            this.users = new Dictionary<string, string>();
            this.users.Add("jortega", "12345");
            this.users.Add("sfernandez", "12345");
            this.users.Add("jrojas", "12345");
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            string user = txtUsuario.Text;
            string password = txtPassword.Text;

            //valida el login
            if (String.IsNullOrEmpty(user) != true && String.IsNullOrEmpty(password) != true)
            {
                if (this.users.ContainsKey(user) && this.users.ContainsValue(password))
                {
                    MessageBox.Show($"Bienvenido {user}", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    PrincipalMDI principal = new PrincipalMDI();
                    principal.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Credenciales incorrectas o el usuario no existe", "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            else
            {
                MessageBox.Show("Ingrese sus credenciales", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
