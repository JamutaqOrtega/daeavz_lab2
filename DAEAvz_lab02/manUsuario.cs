﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAEAvz_lab02
{
    public partial class manUsuario : Form
    {
        //atributos
        List<string> formData;
        int rowIndex;

        public manUsuario()
        {
            InitializeComponent();
            this.formData = new List<string>();
            this.rowIndex = -1;
        }

        private void fetchDataFromForm()
        {
            //limpia la lista
            this.formData.Clear();

            //asigna los datos ingresados en el formulario a la lista
            this.formData.Add(txtDni.Text);
            this.formData.Add(txtNombre.Text);
            this.formData.Add(txtApellido.Text);
            this.formData.Add(txtTelefono.Text);
            this.formData.Add(txtEmail.Text);
            this.formData.Add(txtDireccion.Text);
            this.formData.Add(dtpFechanac.Text);
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            //obtiene los datos del form
            fetchDataFromForm();

            //asigna los datos a una fila de la tabla
            dgvUsuarios.Rows.Add(this.formData[0], this.formData[1], this.formData[2], this.formData[3], this.formData[4], this.formData[5], this.formData[6]);
            this.clearForm(); //limpia los textboxs
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (this.rowIndex != -1) //si hay un indice de columna seteado a modificar
            {
                //obtiene los datos del form
                fetchDataFromForm();

                //aplica los cambios
                for (int i = 0; i < 7; i++) //asigna los datos modificados en el form a las variables
                {
                    dgvUsuarios.Rows[this.rowIndex].Cells[i].Value = this.formData[i];
                }

                //resetea el index
                this.rowIndex = -1;

                MessageBox.Show("Datos actualizados", "Modificar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.clearForm(); //limpia los textboxs
            }
            else
            {
                MessageBox.Show("Seleccione una fila a modificar primero (doble click)", "Modificar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvUsuarios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            //obtiene todos los datos de las celdas seleccionadas y el indice de la fila
            var datos = dgvUsuarios.CurrentRow.Cells;
            this.rowIndex = dgvUsuarios.CurrentRow.Index;

            //asigna los datos del tabla a los campos del formulario
            txtDni.Text = datos[0].Value.ToString();
            txtNombre.Text = datos[1].Value.ToString();
            txtApellido.Text = datos[2].Value.ToString();
            txtTelefono.Text = datos[3].Value.ToString();
            txtEmail.Text = datos[4].Value.ToString();
            txtDireccion.Text = datos[5].Value.ToString();
            dtpFechanac.Text = datos[6].Value.ToString();
        }

        private void clearForm()
        {
            txtDni.Clear();
            txtNombre.Clear();
            txtApellido.Clear();
            txtTelefono.Clear();
            txtEmail.Clear();
            txtDireccion.Clear();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (this.rowIndex != -1) //si hay un indice de columna seteado a modificar
            {
                //elimina una fila especificada
                dgvUsuarios.Rows.RemoveAt(this.rowIndex);

                //resetea el index
                this.rowIndex = -1;

                MessageBox.Show("Fila eliminada", "Eliminar", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.clearForm(); //limpia los textboxs
            }
            else
            {
                MessageBox.Show("Seleccione una fila primero (doble click)", "Eliminar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
